package com.danielshalev.scrapingworker.model;

/**
 * A pojo for app data
 */
public class AppData {

    private String title;
    private String icon;
    private String email;
    private String packageName;
    private String proxyUsed;

    public AppData(String title, String icon, String email, String packageName, String proxyUsed) {
        this.title = title;
        this.icon = icon;
        this.email = email;
        this.packageName = packageName;
        this.proxyUsed = proxyUsed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getProxyUsed() {
        return proxyUsed;
    }

    public void setProxyUsed(String proxyUsed) {
        this.proxyUsed = proxyUsed;
    }

    @Override
    public String toString() {
        return "AppData{" +
                "title='" + title + '\'' +
                ", icon='" + icon + '\'' +
                ", email='" + email + '\'' +
                ", packageName='" + packageName + '\'' +
                ", proxyUsed='" + proxyUsed + '\'' +
                '}';
    }
}
