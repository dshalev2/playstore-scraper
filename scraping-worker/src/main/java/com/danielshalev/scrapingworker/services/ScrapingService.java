package com.danielshalev.scrapingworker.services;


public interface ScrapingService {
    void scrapeWithProxy(String packageName, String ip);
}
