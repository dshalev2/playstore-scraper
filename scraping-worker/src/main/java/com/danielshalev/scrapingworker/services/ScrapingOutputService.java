package com.danielshalev.scrapingworker.services;

import com.danielshalev.scrapingworker.model.AppData;

/**
 * An interface for scraping putput
 */
public interface ScrapingOutputService {

    /**
     * write the app data to the output
     *
     * @param appData the app data
     */
    void write(AppData appData);

    /**
     * write error to the output
     * @param error the error message
     */
    void writeError(String error);
}
