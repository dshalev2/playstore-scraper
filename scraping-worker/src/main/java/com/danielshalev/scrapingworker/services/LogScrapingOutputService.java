package com.danielshalev.scrapingworker.services;

import com.danielshalev.scrapingworker.ScrapingWorkerApplication;
import com.danielshalev.scrapingworker.model.AppData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * scraping output service that writes the output to a log
 */
@Component
public class LogScrapingOutputService implements ScrapingOutputService {

    private static final Logger logger = LoggerFactory.getLogger(ScrapingWorkerApplication.class);

    @Override
    public void write(AppData appData) {
        logger.info("data scraped: " + appData.toString());
    }

    @Override
    public void writeError(String error) {
        logger.info("unable to scrape error: " + error);
    }
}
