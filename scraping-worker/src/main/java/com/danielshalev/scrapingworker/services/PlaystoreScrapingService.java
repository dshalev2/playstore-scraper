package com.danielshalev.scrapingworker.services;

import com.danielshalev.scrapingworker.model.AppData;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A scraping service for google playstore
 */
@Service
public class PlaystoreScrapingService implements ScrapingService {

    private static final String URL = "https://play.google.com/store/apps/details?id=";
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36";

    public static final String TITLE_SELECTOR = "meta[property=og:title]";
    public static final String ICON_SELECTOR = "img[alt='Cover art']";
    public static final String EMAIL_SELECTOR = "a[href^=mailto]";

    @Autowired
    private ScrapingOutputService scrapingOutputService;

    @Override
    public void scrapeWithProxy(String packageName, String ip) {
        try {
            PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
            connectionManager.setValidateAfterInactivity(500);


            CloseableHttpClient httpClient = HttpClients.custom()
                    .setConnectionManager(connectionManager)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setSslcontext(new SSLContextBuilder().loadTrustMaterial(null, (x509Certificates, s) -> true).build())
                    .setUserAgent(USER_AGENT)
                    .build();

            HttpGet request = new HttpGet(URL + packageName);
            HttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                scrapingOutputService.writeError("page not found for app id: " + packageName);
                return;
            }

            String content = EntityUtils.toString(response.getEntity());

            Document doc = Jsoup.parse(content);

            Element titleElement = doc.select(TITLE_SELECTOR).first();
            Element iconElement = doc.selectFirst(ICON_SELECTOR);

            if (titleElement == null || iconElement == null) {
                scrapingOutputService.writeError("page not found for app is: " + packageName);
            }

            String title = titleElement.attr("content") != null ? titleElement.attr("content") : "";
            String icon = iconElement.attr("src") != null ? iconElement.attr("src") : "";

            String email = "";
            Element emailElement = doc.select(EMAIL_SELECTOR).first();
            if (emailElement != null) {
                String[] splitedHref = emailElement.attr("href").split(":");
                if (splitedHref.length == 2) {
                    email = splitedHref[1];
                }
            }

            AppData appData = new AppData(title, icon, email, packageName, ip);
            scrapingOutputService.write(appData);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
