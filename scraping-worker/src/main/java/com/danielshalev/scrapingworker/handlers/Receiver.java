package com.danielshalev.scrapingworker.handlers;

import com.danielshalev.avro.ScrapeWithProxyRequest;
import com.danielshalev.scrapingworker.services.ScrapingService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

public class Receiver {

    @Autowired
    private ScrapingService scrapingService;

    @KafkaListener(topics = "${kafka.topic.scrape_with_proxy}")
    public void receive(ScrapeWithProxyRequest scrapeWithProxyRequest) {
        scrapingService.scrapeWithProxy(scrapeWithProxyRequest.getAppIdentifier().toString(), scrapeWithProxyRequest.getIp().toString());
    }
}
