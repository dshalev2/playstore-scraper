package com.danielshalev.scrapingworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScrapingWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScrapingWorkerApplication.class, args);
	}
}
