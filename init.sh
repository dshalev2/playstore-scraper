#!/bin/bash

mvn clean install -Dmaven.test.skip=true
docker-compose down && docker-compose build --no-cache && docker-compose up -d
docker-compose run wait-for-kafka
docker-compose restart scraper-gateway
docker-compose restart scraping-proxy-balancer
docker-compose restart scraping-worker

# docker exec -it similarweb_kafka_1 bash -c "bin/kafka-topics.sh --create --replication-factor 1 --partitions 1 --topic scrape_with_proxy_request"
# docker exec -it similarweb_kafka_1 bash -c "bin/kafka-topics.sh --create --replication-factor 1 --partitions 1 --topic scrape_request"
# docker exec -it similarweb_kafka_1 bash -c "bin/kafka-topics.sh --create --replication-factor 1 --partitions 1 --topic free_proxy"