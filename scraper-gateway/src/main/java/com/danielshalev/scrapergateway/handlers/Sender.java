package com.danielshalev.scrapergateway.handlers;

import com.danielshalev.avro.ScrapeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class Sender {

    @Value("${kafka.topic.scrape_request}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, ScrapeRequest> kafkaTemplate;

    /**
     * produces a message to a kafka topic
     *
     * @param scrapeRequest the message payload
     */
    public Boolean send(ScrapeRequest scrapeRequest) {
        kafkaTemplate.send(topic, scrapeRequest);
        return true;
    }
}
