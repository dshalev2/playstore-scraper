package com.danielshalev.scrapergateway.controllers;

import com.danielshalev.avro.ScrapeRequest;
import com.danielshalev.scrapergateway.handlers.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ScraperGatewayController {

    @Autowired
    private Sender messageSender;

    @RequestMapping(value = "/scrape", method = RequestMethod.POST)
    public void scrape(@RequestBody ScrapeRequest scrapeRequest) {
        messageSender.send(scrapeRequest);
    }
}
