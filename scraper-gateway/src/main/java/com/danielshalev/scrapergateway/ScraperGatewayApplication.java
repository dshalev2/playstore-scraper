package com.danielshalev.scrapergateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScraperGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScraperGatewayApplication.class, args);
	}
}
