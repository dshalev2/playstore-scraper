package com.danielshalev.scrapingproxybalancer.handlers;


import com.danielshalev.avro.ScrapeRequest;
import com.danielshalev.avro.ScrapeWithProxyRequest;
import com.danielshalev.scrapingproxybalancer.services.ProxyAllocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

public class Receiver {

    @Autowired
    private ProxyAllocationService proxyAllocationService;

    @Autowired
    private Sender sender;

    @KafkaListener(topics = "${kafka.topic.scrape_request}")
    public void receiveScrapeRequest(ScrapeRequest scrapeRequest) {
        while (true) {
            if (proxyAllocationService.hasAvailableProxy()) {
                ScrapeWithProxyRequest scrapeWithProxyRequest = new ScrapeWithProxyRequest();
                scrapeWithProxyRequest.setAppIdentifier(scrapeRequest.getAppIdentifier().toString());
                scrapeWithProxyRequest.setIp(proxyAllocationService.getAvailableProxy().getIp());

                sender.send(scrapeWithProxyRequest);
                proxyAllocationService.freeProxy(scrapeWithProxyRequest.getIp().toString());

                break;
            }
        }
    }
}
