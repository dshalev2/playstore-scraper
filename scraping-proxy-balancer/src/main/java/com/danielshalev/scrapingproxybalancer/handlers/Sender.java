package com.danielshalev.scrapingproxybalancer.handlers;

import com.danielshalev.avro.ScrapeWithProxyRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class Sender {

    @Value("${kafka.topic.scrape_with_proxy_request}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, ScrapeWithProxyRequest> kafkaTemplate;

    public void send(ScrapeWithProxyRequest scrapeWithProxyRequest) {
        kafkaTemplate.send(topic, scrapeWithProxyRequest);
    }
}
