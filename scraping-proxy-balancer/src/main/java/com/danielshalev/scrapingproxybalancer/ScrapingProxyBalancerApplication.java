package com.danielshalev.scrapingproxybalancer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScrapingProxyBalancerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScrapingProxyBalancerApplication.class, args);
	}
}
