package com.danielshalev.scrapingproxybalancer.config;

import com.danielshalev.scrapingproxybalancer.model.ProxyDestination;
import com.danielshalev.scrapingproxybalancer.services.ProxyDestinationComperator;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

@Configuration
public class ProxyAllocationConfig {

    @Value("#{'${proxy.ip.list}'.split(',')}")
    private List<String> proxyIps;

    @Bean
    public Queue<ProxyDestination> proxyDestinationQueue() {
        Queue<ProxyDestination> queue = new PriorityQueue<>(50, new ProxyDestinationComperator());
        proxyIps.forEach(ip -> queue.add(new ProxyDestination(ip, DateTime.now())));

        return queue;
    }
}
