package com.danielshalev.scrapingproxybalancer.model;


import org.joda.time.DateTime;

/**
 * a pojo for proxy destination
 */
public class ProxyDestination {

    private String ip;

    private DateTime lastUsed;

    public ProxyDestination() {

    }

    public ProxyDestination(String ip, DateTime lastUsed) {
        this.ip = ip;
        this.lastUsed = lastUsed;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public DateTime getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(DateTime lastUsed) {
        this.lastUsed = lastUsed;
    }
}
