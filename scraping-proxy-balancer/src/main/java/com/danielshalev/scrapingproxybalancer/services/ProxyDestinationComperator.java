package com.danielshalev.scrapingproxybalancer.services;

import com.danielshalev.scrapingproxybalancer.model.ProxyDestination;

import java.util.Comparator;

/**
 * A comperator to be used by a proxy destination priority queue
 */
public class ProxyDestinationComperator implements Comparator<ProxyDestination> {

    @Override
    public int compare(ProxyDestination o1, ProxyDestination o2) {
        return o1.getLastUsed().compareTo(o2.getLastUsed());
    }
}
