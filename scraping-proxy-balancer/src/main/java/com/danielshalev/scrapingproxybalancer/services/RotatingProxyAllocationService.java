package com.danielshalev.scrapingproxybalancer.services;

import com.danielshalev.scrapingproxybalancer.model.ProxyDestination;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Queue;

/**
 * Round robin rotating proxy allocation service implementation
 */
@Service
public class RotatingProxyAllocationService implements ProxyAllocationService {

    /**
     * A queue of proxies who are not allocated
     */
    @Autowired
    private Queue<ProxyDestination> availableProxies;

    @Override
    public Boolean hasAvailableProxy() {
        if (availableProxies.isEmpty()) {
            return false;
        }

        return DateTime.now().isAfter(availableProxies.peek().getLastUsed().plusSeconds(10));
    }

    @Override
    public ProxyDestination getAvailableProxy() {
        return availableProxies.poll();
    }

    @Override
    public void freeProxy(String ip) {
        ProxyDestination proxyDestination = new ProxyDestination(ip, DateTime.now());

        availableProxies.add(proxyDestination);
    }
}
