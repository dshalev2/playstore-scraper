package com.danielshalev.scrapingproxybalancer.services;

import com.danielshalev.avro.ScrapeWithProxyRequest;
import com.danielshalev.scrapingproxybalancer.model.ProxyDestination;

/**
 * an interface for proxy allocation service
 */
public interface ProxyAllocationService {

    /**
     *
     * @return true if there are available proxys, false O.W
     */
    Boolean hasAvailableProxy();

    /**
     *
     * @return an available proxy destination
     */
    ProxyDestination getAvailableProxy();

    /**
     * adds a free proxy to the allocation
     *
     * @param ip the proxy to free
     */
    void freeProxy(String ip);
}
