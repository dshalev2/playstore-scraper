## Playstore Scraper

### Running Instructions:

#### mac:

1. install homebrew `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
2. install docker and maven `brew tap caskroom/cask; brew cask install docker; brew install maven`
3. clone repository: `git clone https://gitlab.com/dshalev2/playstore-scraper.git`
4. run the following instructions:
	```
	cd playstore-scraper
	mvn clean install
	./init.sh
	```
	This will build containers for all components
5. install golang `brew install golang`
6.  in order to send all apps in the file to the scraper:
	```
	go build scraping-util/main.go
	./main -file=appIds.txt
	```
	change the file path for different files

#### app data will be outputed to logs
	
* logs can be accessed with thi command: `docker exec -it playstorescraper_scraping-worker_1 bash -c "tail -f /tmp/logs/scraping.log"`
* logs can be copied to your directory using: `docker cp playstorescraper_scraping-worker_1:/tmp/logs/scraping.log .`