package main

import (
	"flag"
	"os"
	"fmt"
	"bufio"
	"encoding/json"
	"net/http"
	"bytes"
)

const url = "http://127.0.0.1:8080/scrape"

type RequestBody struct {
	appIdentifier string
}

func main() {
	fileFlag := flag.String("file", "", "path to package names file")
	flag.Parse()
	path := *fileFlag
	if path == "" {
		flag.Usage()
		os.Exit(1)
	}
	handleFileAtPath(path)
}

func handleFileAtPath(path string) {
	file, err := os.OpenFile(path, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("open file error: %v", err)
		os.Exit(1)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		callScrapingService(scanner.Text())
	}
}


func callScrapingService(packageName string) {
	body := map[string]string{"appIdentifier": packageName}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		fmt.Println("json create error: %v", err)
		os.Exit(1)
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonBody))
	if err != nil {
		fmt.Println("http request create error: %v", err)
		os.Exit(1)
	}
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("http request error: %v", err)
		os.Exit(1)
	}
	if resp.StatusCode != 200 {
		fmt.Println("scraping service returnd an error %s", string(jsonBody))
		os.Exit(1)
	}
}